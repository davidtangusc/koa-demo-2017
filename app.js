require('dotenv').config();

var app = require('koa')();
var router = require('koa-router')();
var cors = require('koa-cors');
var bodyParser = require('koa-bodyparser');
var Song = require('./models/song');

router.use(cors());
router.use(bodyParser());

router.get('/api/songs', function*() {
  var songs = yield Song.fetchAll();
  this.body = songs;
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);

