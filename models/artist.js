var bookshelf = require('./../bookshelf');
var Song = require('./song');

var Artist = bookshelf.Model.extend({
  tableName: 'artists',
  songs: function() {
    return this.hasMany(Song);
  }
});

module.exports = Artist;